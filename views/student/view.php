<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'View Student';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
		<?php echo "the name of the student $name" ?>
    </p>

    <code><?= __FILE__ ?></code>
</div>
