<?php

namespace app\controllers;
use app\models\Student;
use Yii;
use yii\web\Controller;


class StudentController extends Controller
{
	    public function actionView($id)
		{
        $name = Student::getName($id);
		return $this->render('view',['name'=>$name]);
		}
	
	
}
