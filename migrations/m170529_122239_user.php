<?php

use yii\db\Migration;

class m170529_122239_user extends Migration
{
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
			'authKey' => $this->string()->notNull(),
			'password' => $this->string()->notNull(),
               ]);
    }
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
