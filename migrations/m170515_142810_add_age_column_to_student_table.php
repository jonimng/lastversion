<?php

use yii\db\Migration;

/**
 * Handles adding age to table `student`.
 */
class m170515_142810_add_age_column_to_student_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('student','age', $this->integer());
		$this->addColumn('student','id_num', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('student', 'id_num');
    }
}
