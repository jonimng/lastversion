<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii;

class User extends  ActiveRecord  implements \yii\web\IdentityInterface
{
	public $role;
	
	public static function  tableName()
	{
		return 'user'; // חיבור לטבלת משתמשיםפ
	}
	
	public  function rules()
	{	
		
		return [
			[['username','password','authKay'],'string','max' => 255],
			[['username','password'],'required'],
			[['username'],'unique'],
			['role', 'safe'],
		];
	
		
	}
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findone($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
       throw new NotSupportedException('Not supported');

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
			return self::findOne(['username' => $username]);
    }

	public function getFullname()
    {
			return $this->firstname.' '.$this->lastname;
    }
	
	public static function getUsers()
	{
		$users = ArrayHelper::
					map(self::find()->all(), 'id', 'fullname');///map [הינה פונקציה אשר שולפת מידע בצורה של מערכים לדוגמא:[1.יוני]
		return $users;						
	}
	
	public static function getRoles()
	{

		$rolesObjects = Yii::$app->authManager->getRoles();
		$roles = [];
		foreach($rolesObjects as $id =>$rolObj){
			$roles[$id] = $rolObj->name; 
		}
		
		return $roles; 		
	}


  

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKay;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKay)
    {
        return $this->authKay === $authKay;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
     public function validatePassword($password)
    {
        return $this->isCorrectHash($password, $this->password);;
    }
	
	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}
	
	
	//hash password before saving
	public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password);

        if ($this->isNewRecord)
		    $this->authKay = Yii::$app->security->generateRandomString(32);

        return $return;
    }
	
	public function afterSave($insert,$changedAttributes)
    {
        $return = parent::afterSave($insert, $changedAttributes);

		$auth = Yii::$app->authManager;
		$roleName = $this->role; 
		$role = $auth->getRole($roleName);
		if (\Yii::$app->authManager->getRolesByUser($this->id) == null){
			$auth->assign($role, $this->id);
		} else {
			$db = \Yii::$app->db;
			$db->createCommand()->delete('auth_assignment',
				['user_id' => $this->id])->execute();
			$auth->assign($role, $this->id);
		}

        return $return;
    }	

	
}
